#-*- coding: utf-8 -*-


# Prune the first X and/or the last Y base pairs
# Files must be in fasta(.gz) or fastq(.gz) format
# Identifiers, sequences and PHRED values must occupy one
# line each

# Author: Denis Jacob Machado
# Email: denisjacobmachado@gmail.com
# Homepage: http://www.ib.usp.br/grant/

# Copyright (C) 2014 - Denis Jacob Machado
# Licence: GNU General Public License version 3.0
def execute(ARGS_input,ARGS_output,ARGS_head,ARGS_tail,ARGS_stanza_size,ARGS_verbose,ARGS_gzip):
	if(ARGS_verbose):print("PRUNER")

# Import libraries

	try:import re
	except ImportError:
		print(">Could not import re")
		exit()
	try:from itertools import islice
	except ImportError:
		print(">ERROR: NOT FOUND: islice in itertools")
		exit()

# Read input file

	for f in range(0,len(ARGS_input)):
		if(f>=1):
			ARGS_output=list(re.compile("([^.]+)\.(.+)").findall(ARGS_output)[0])
			ARGS_output="%s_%d.%s"%(ARGS_output[0],f+1,ARGS_output[1])
		file=str(ARGS_input[f])
		if(ARGS_verbose):print(">Readinf from %s"%(file))
		if(ARGS_gzip):
			try:SOURCE=gzip.open(file,"ra")
			except:
				print(">ERROR: reading %s"%(file))
				exit()
		else:
			try:SOURCE=open(file,"r")
			except:
				print(">ERROR: reading %s"%(file))
				exit()

# Open output file

		if(ARGS_verbose):print(">Creating %s"%(ARGS_output))
		if(ARGS_gzip):
			try:OUTPUT=gzip.open(ARGS_output,"wa")
			except:
				print(">ERROR: reading %s"%(ARGS_output))
				exit()
		else:
			try:OUTPUT=open(ARGS_output,"w")
			except:
				print(">ERROR: reading %s"%(ARGS_output))
				exit()

# General variables

		SORTED=True
		CONTINUE=True
		stanza_count=0
		last_header=""
		length_before=[]
		length_after=[]
		CHUNK=[]

# Read stanzas

		while CONTINUE==True:
			STANZA=list(islice(SOURCE,ARGS_stanza_size))
			if(STANZA):
				stanza=STANZA[0]
				stanza_count+=1
				if(SORTED==True)and(stanza.rstrip()>last_header):last_header=stanza.rstrip()
				else:SORTED=False
				for i in range(1,len(STANZA)):
					if(i%2!=0):
						line=STANZA[i].rstrip()
						length_before+=[len(line)]
						line=line[ARGS_head:len(line)-ARGS_tail]
						length_after+=[len(line)]
						stanza+=line+"\n"
					else:
						stanza+=STANZA[i]
				OUTPUT.write(stanza)
			else:
				CONTINUE==False
				break

# Final messages

		if(ARGS_verbose):
			print(">Stanza count = %d\n>Average sequence size (before)= %.2f\n>Average sequence size (after)= %.2f"%(stanza_count,sum(length_before)/float(len(length_before)),sum(length_after)/float(len(length_after))))
		if(ARGS_verbose)and(SORTED==True):print(">File %s is sorted by sequence identifiers"%(file))
		elif(ARGS_verbose)and(SORTED==False):print(">File %s is NOT sorted by sequence identifiers"%(file))
		SOURCE.close()
		OUTPUT.close()
		if(ARGS_verbose):print(">Done!")