#!/usr/bin/python
#-*- coding: utf-8 -*-


# PAckage of TOols with Fast Utilities

# Author: Denis Jacob Machado
# Email: denisjacobmachado@gmail.com
# Homepage: http://www.ib.usp.br/grant/

# Copyright (C) 2014 - Denis Jacob Machado
# Licence: GNU General Public License version 3.0

print"""
PAckage of TOols with Fast Utilities (PATO-FU)
Copyright (C) 2014 - Denis Jacob Machado
GNU General Public License version 3.0
"""

# Try to import the necessary libraries
try:import argparse
except ImportError:
	print(">Could not import argparse")
	exit()
try:import os
except ImportError:
	print(">Could not import os")
	exit()
try:import re
except ImportError:
	print(">Could not import re")
	exit()
try:import sys
except ImportError:
	print(">Could not import sys")
	exit()


parser=argparse.ArgumentParser()

# Tools

parser.add_argument("-P","--pruner",help="prune sequences",action="store_true",default=False)
parser.add_argument("-S","--sorter",help="sort sequences by identifier",action="store_true",default=False)
parser.add_argument("-R","--randomizer",help="collect random samples from a sequence file",action="store_true",default=False)
parser.add_argument("-L","--interleaver",help="concatenate two sequence files in a interleaves manner",action="store_true",default=False)
parser.add_argument("-D","--deinterleaver",help="increase output verbosity (default = False)",action="store_true",default=False)
parser.add_argument("--suffixes",help="edit sequence identifiers (default = False)",action="store_true",default=False)
parser.add_argument("-I","--identifier",help="change the format of the sequence identifiers (default = False)",action="store_true",default=False)
parser.add_argument("-C","--counter",help="count the number of sequences (default = False)",action="store_true",default=False)
parser.add_argument("-E","--extractor",help="extracts gunzipped files (default = False)",action="store_true",default=False)
parser.add_argument("--checker",help="verifies if reads are paired and/or sorted (default = False)",action="store_true",default=False)

# Boolean arguments

parser.add_argument("-fa","--fasta",help="input in fasta format",action="store_true",default=False)
parser.add_argument("-fq","--fastq",help="input in fastq format (default)",action="store_true",default=True)
parser.add_argument("-v","--verbose",help="increase output verbosity (default = False)",action="store_true",default=False)
parser.add_argument("-z","--gzip",help="gzip compressed files (default = False)",action="store_true",default=False)

parser.add_argument("-A","--available",help="print available functions and the arguments each of them take (default = False)",action="store_true",default=False)
parser.add_argument("--suffix_removal",help="remove suffixes (default = False)",action="store_true",default=False)

# Other arguments

parser.add_argument("-i","--input",help="list of input files",nargs='+',type=str)
parser.add_argument("-o","--output",help="output file prefix (default = output)",metavar="<STRING>",default="output")
parser.add_argument("-H","--head",help="bases to prune from sequence head (default = 10)",metavar="<INT>",type=int,default=10)
parser.add_argument("-T","--tail",help="bases to prune from sequence tail (default = 0)",metavar="<INT>",type=int,default=0)
parser.add_argument("-ck","--chunk_size",help="number of reads per chunk (default = 100)",metavar="<INT>",type=int,default=100)
parser.add_argument("-sz","--stanza_size",help="number of lines per stanza (default = fasta: 2, fastq: 4)",metavar="<INT>",type=int)
parser.add_argument("-p","--probability",help="probability of selecting a read",metavar="<0.0 to 1.0>",type=float,default=0.01)
parser.add_argument("-m","--max",help="maximum number of reads to be selected (set 0 for no threshold)",metavar="<INT>",type=int,default=0)
parser.add_argument("-sl","--list_of_suffixes",help="list of sufifixes",nargs='+',type=str,default=['/1','/2'])

parser.add_argument("--line_break",help="define the end of a line (default = '\n')",metavar="<STRING>",default="\n")
parser.add_argument("--checker_opt",help="0: single reads; 1: interleaved reads; 2: paired reads in two separated files (default = 0)",metavar="<0, 1 or 2>",type=int,default=0)

args=parser.parse_args()

# Missing function

if(args.available)or(args.pruner==args.sorter==args.randomizer==args.interleaver==args.deinterleaver==args.identifier==args.extractor==args.counter==args.checker==False):
	print"""
AVAILABLE OPTIONS:

	"-P", "--pruner": prune sequences
		Takes the following arguments:
		--input <File_1 File_2 ... File_n>
		--output <STRING>
		--head <INTEGER>
		--tail <INTEGER>
		--stanza_size <INTEGER>
		--verbose
		--gzip
	
	"-S", "--sorter": sort sequences by identifier
		Takes the following arguments:
		--input <File_1 File_2 ... File_n>
		--output <STRING>
		--chunk_size <INTEGER>
		--stanza_size <INTEGER>
		--verbose
		--gzip

	"-R", "--randomizer": collect random samples from a sequence file
		Takes the following arguments:
		--input <File_1 File_2 ... File_n>
		--output <STRING>
		--probability <0.0 to 1.0>
		--max <INTEGER>
		--verbose
		--gzip

	"-L", "--interleaver": concatenate two sequence files in a interleaves manner
		Takes the following arguments:
		--input <File_1 File_2 ... File_n>
		--output <STRING>
		--stanza_size <INTEGER>
		--verbose
		--gzip

	"-D", "--deinterleaver": increase output verbosity (default = False)
		Takes the following arguments:
		--input <File_1 File_2 ... File_n>
		--output <STRING>
		--stanza_size <INTEGER>
		--verbose
		--gzip

	"--suffixes": edit sequence identifiers (default = False)
		Takes the following arguments:
		--input <File_1 File_2 ... File_n>
		--output <STRING>
		--stanza_size <INTEGER>
		--list_of_suffixes <STRING>
		--verbose
		--gzip
		--suffix_removal
		--line_break <STRING>

	"-I", "--identifier": change the format of the sequence identifiers (default = False)
		Takes the following arguments:
		--input <File_1 File_2 ... File_n>
		--output <STRING>
		--stanza_size <INTEGER>
		--verbose
		--gzip

	"-C", "--counter": count the number of sequences (default = False)
		Takes the following arguments:
		--input <File_1 File_2 ... File_n>
		--stanza_size <INTEGER>
		--verbose
		--gzip

	"-E", "--extractor": extracts gunzipped files (default = False)
		Takes the following arguments:
		--input <File_1 File_2 ... File_n>
		--output <STRING>
		--verbose

	"--checker": 0: single reads; 1: interleaved reads; 2: paired reads in two separated files (default = 0)
		Takes the following arguments:
		--input <File_1 File_2 ... File_n>
		--stanza_size <INTEGER>
		--verbose
		--gzip
		--checker_opt <0, 1 or 2>

"""
	exit()

# Resolve output file name

args.output=re.compile("([^.]+)\.*").findall(args.output)[0]
if(args.fasta):args.output+=".fasta"
elif(args.fastq):args.output+=".fastq"
if(args.gzip)and(not args.extractor):
	try:import gzip
	except:
		print(">ERROR: NOT FOUND: gzip")
		exit()
	else:args.output+=".gz"
	

# Chech input files

if(args.input):
	for f in args.input:
		try:os.path.isfile(f)
		except:
			print(">ERROR: file %s not found"%(f))
			exit()
else:
	print(">ERROR: no input files were found. Check the documentation for further information")
	exit()

# Resolve stanza size

if(not args.stanza_size):
	if(args.fasta):args.stanza_size=2
	elif(args.fastq):args.stanza_size=4

# 1. Execute pruner

if(args.pruner):
	try:import fu_pruner
	except:
		print(">ERROR: could not find fu_pruner.py")
		exit()
	else:fu_pruner.execute(args.input,args.head,args.tail,args.stanza_size,args.verbose,args.gzip)

# 2. Execute sorter

elif(args.sorter):
	try:import fu_sorter
	except:
		print(">ERROR: could not find fu_sorter.py")
		exit()
	else:fu_sorter.execute(args.input,args.output,args.chunk_size,args.stanza_size,args.verbose,args.gzip)

# 3. Execute randomizer

elif(args.randomizer)and(args.probability<=1.0)and(args.probability>=0.0)and(args.max>=0):
	try:import fu_randomizer
	except:
		print(">ERROR: could not find fu_randomizer.py")
		exit()
	else:
		print(args.input,args.output,args.probability,args.max,args.verbose,args.gzip)
		fu_randomizer.execute(args.input,args.output,args.probability,args.max,args.verbose,args.gzip)

# 4. Execute interleaver

elif(args.interleaver):
	try:import fu_interleaver
	except:
		print(">ERROR: could not find fu_interleaver.py")
		exit()
	else:
		if(len(args.input)==2):
			fu_interleaver.execute(args.input,args.output,args.stanza_size,args.verbose,args.gzip)
		else:
			print(">ERROR: The fu_interleaver takes exactly 2 input files, %d given"%(len(ARGS_input)))
			exit()

# 5. Execute deinterleaver

elif(args.deinterleaver):
	try:import fu_deinterleaver
	except:
		print(">ERROR: could not find fu_deinterleaver.py")
		exit()
	else:
		if(len(args.input)==1):
			fu_deinterleaver.execute(args.input,args.output,args.stanza_size,args.verbose,args.gzip)
		else:
			print(">ERROR: The fu_deinterleaver takes exactly 1 input file, %d given"%(len(ARGS_input)))
			exit()

# 6. Execute suffixes

elif(args.suffixes):
	if(len(args.input)==len(args.suffix)):
		try:import fu_suffixes
		except:
			print(">ERROR: could not find fu_suffixes.py")
			exit()
		else:fu_identifier.execute(args.input,args.output,args.stanza_size,args.suffix,args.verbose,args.gzip,args.suffix_removal,args.line_break)
	else:
		print(">Error: fu_suffixes: the number of input files must be the same as the number of suffixes")
		exit()

# 7. Execute identifier

elif(args.identifier):
	try:import fu_identifier
	except:
		print(">ERROR: could not find fu_identifier.py")
		exit()
	else:fu_identifier.execute(args.input,args.output,args.stanza_size,args.verbose,args.gzip)

# 8. Counter

elif(args.counter):
	try:import fu_counter
	except:
		print(">ERROR: could not find fu_counter.py")
		exit()
	else:fu_counter.execute(args.input,args.stanza_size,args.verbose,args.gzip)

# 9. Extractor

elif(args.extractor):
	try:import fu_extractor
	except:
		print(">ERROR: could not find fu_extractor.py")
		exit()
	else:fu_extractor.execute(args.input,args.output,args.verbose)

# 10. Checker

elif(args.checker):
	try:import fu_checker
	except:
		print(">ERROR: could not find fu_checker.py")
		exit()
	else:fu_checker.execute(args.input,args.verbose,args.checker_opt,args.stanza_size,args.gzip)

exit()