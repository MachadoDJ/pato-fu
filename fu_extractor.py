#-*- coding: utf-8 -*-

# Extracts fastq.gz files into fastq files

# Author: Denis Jacob Machado
# Email: denisjacobmachado@gmail.com
# Homepage: http://www.ib.usp.br/grant/

# Copyright (C) 2014 - Denis Jacob Machado
# Licence: GNU General Public License version 3.0

def execute(ARGS_input,ARGS_output,ARGS_verbose):
	if(ARGS_verbose):print("RANDOMIZER\n>Extracting gunzipped files")

# Import libraries

	try:import re
	except ImportError:
		print(">ERROR: NOT FOUND: re")
		exit()
	try:from itertools import islice
	except ImportError:
		print(">ERROR: NOT FOUND: itertools:islice")
		exit()
	try:import gzip
	except ImportError:
		print(">ERROR: NOT FOUND: gzip")
		exit()

# Get output name and read from input

	for f in range(0,len(ARGS_input)):
		file=str(ARGS_input[f])
		if(f>=1):
			ARGS_output=list(re.compile("([^.]+)\.(.+)").findall(ARGS_output)[0])
			outname="%s_%d.%s"%(ARGS_output[0],f+1,ARGS_output[1])
		else:outname=ARGS_output
		try:INPUT=gzip.open(file,"rb")
		except:
			print(">ERROR: reading %s"%(file))
			exit()
		try:OUTPUT=open(outname,"w")
		except:
			print(">ERROR: reading %s"%(file))
			exit()
		if(ARGS_verbose):print("--Extracting %s into %s"%(file,outname))
		for line in INPUT.readlines():OUTPUT.write(line)
		INPUT.close()
		OUTPUT.close()
		if(ARGS_verbose):print(">Done!")