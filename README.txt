PATO-FU - Copyright (C) 2014 - Denis Jacob Machado

PATO-FU comes with ABSOLUTELY NO WARRANTY!

AVAILABILITY

PATO-FU is available at
www.ib.usp.br/grant/anfibios/researchSoftware.html.

LICENSE

The programs may be freely used, modified, and shared under
the GNU General Public License version 3.0 (GPL-3.0,
http://opensource.org/licenses/GPL-3.0). See LICENCE.txt

DESCRIPTION

The PAckage of TOols with Fast(A/Q) Utilities (PATO-FU) is a
collection of homemade Python scripts take can be useful for
dealing with raw sequencing data in FASTA or FASTQ format.
The programs can also accept gunzip'ed files as input.

WARNING NOTES

Input files must contain identifiers, sequences and quality
scores in one line each. For separated paired end sequence
files, each pair must occupy the same line in each file.

USAGE

pato-fu.py [-h] [-P] [-S] [-R] [-L] [-D] [-I] [-C] [-E]
	[-fa] [-fq] [-v] [-z] [-A] [--suffix_removal] [-i INPUT
	[INPUT ...]] [-o <STRING>] [-H <INT>] [-T <INT>] [-ck <INT>]
	[-sz <INT>] [-p <0.0 to 1.0>] [-m <INT>]
	[-su SUFFIX [SUFFIX...]] [--line_break <STRING>]

ALL ARGUMENTS

-h, --help				show this help message and exit
-P, --pruner			prune sequences
-S, --sorter			sort sequences by identifier
-R, --randomizer		collect random samples from sequence
						file
-L, --interleaver		concatenate two sequence files in a
						interleaves manner
-D, --deinterleaver		increase output verbosity
						(default = False)
-I, --identifier		edit sequence identifiers (default =
						False)
-C, --counter			count the number of sequences
						(default = False)
-E, --extractor			extracts gunzipped files (default
						= False)
-fa, --fasta			input in fasta format
-fq, --fastq			input in fastq format (default)
-v, --verbose			increase output verbosity (default
						= False)
-z, --gzip				gzip compressed files (default =
						False)
-A, --available			print available functions and the
						arguments each of them take (default
						= False)
--suffix_removal		remove suffixes (default = False)
-i INPUT [INPUT ...], --input INPUT [INPUT ...]
						list of input files
-o <STRING>, --output <STRING>
						output file prefix (default = output)
-H <INT>, --head <INT>	bases to prune from sequence head
						(default = 10)
-T <INT>, --tail <INT>	bases to prune from sequence tail
						(default = 0)
-ck <INT>, --chunk_size <INT>
						number of reads per chunk (default =
						100)
-sz <INT>, --stanza_size <INT>
						number of lines per stanza (default
						= fasta: 2, fastq: 4)
-p <0.0 to 1.0>, --probability <0.0 to 1.0>
						probability of selecting a read
-m <INT>, --max <INT>
						maximum number of reads to be
						selected (set 0 for no threshold)
-su SUFFIX [SUFFIX ...], --suffix SUFFIX [SUFFIX ...]
						list of sufifixes
--line_break <STRING>
						define the end of a line (default =
						' ')

ARGUMETS PER FUNCTION

-P, --pruner

Prune sequences, removing a given number of characters from
the head and tail of each sequence.
	--input <File 1 ... File n>
	--output <STRING>
	--head <INTEGER>
	--tail <INTEGER>
	--stanza_size <INTEGER>
	--verbose
	--gzip

-S, --sorter

Sort sequences by the sequence identifier.
	--input <File 1 ... File n>
	--output <STRING>
	--chunk_size <INTEGER>
	--stanza_size <INTEGER>
	--verbose
	--gzip


-R, --randomizer

Collect random samples from sequence file.
	--input <File 1 ... File n>
	--output <STRING>
	--probability <0.0 to 1.0>
	--max <INTEGER>
	--verbose
	--gzip

-L, --interleaver

Concatenate two sequence files in a interleaved manner.
	--input <File 1 ... File n>
	--output <STRING>
	--stanza_size <INTEGER>
	--verbose
	--gzip

-D, --deinterleaver

Separates interleaved sequences into two different files.
	--input <File 1 ... File n>
	--output <STRING>
	--stanza_size <INTEGER>
	--verbose
	--gzip

-I, --identifier

Edit sequence identifiers.
	--input <File 1 ... File n>
	--output <STRING>
	--stanza_size <INTEGER>
	--suffix <STRING>
	--verbose
	--gzip
	--suffix_removal
	--line_break <STRING>

-C, --counter

Count the number of sequences (default = False).
	--input <File 1 ... File n>
	--stanza_size <INTEGER>
	--verbose
	--gzip

-E, --extractor

Extracts gunzipped files.
	--input <File 1 ... File n>
	--output <STRING>
	--verbose