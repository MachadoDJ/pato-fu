#-*- coding: utf-8 -*-

# Add an suffix to the identifiers of each read
# Each read must occupy four lines
# Paired reads must be given in two files

# Author: Denis Jacob Machado
# Email: denisjacobmachado@gmail.com
# Homepage: http://www.ib.usp.br/grant/

# Copyright (C) 2014 - Denis Jacob Machado
# Licence: GNU General Public License version 3.0

def execute(ARGS_input,ARGS_output,ARGS_probability,ARGS_max,ARGS_verbose,ARGS_gzip):
	if(ARGS_verbose):print("RANDOMIZER")

# Import libraries

	try:import re
	except ImportError:
		print(">ERROR: NOT FOUND: re")
		exit()
	try:from itertools import islice
	except ImportError:
		print(">ERROR: NOT FOUND: islice in itertools")
		exit()
	try:import random
	except ImportError:
		print(">ERROR: NOT FOUND: random")
		exit()
	if(ARGS_gzip):
		try:import gzip
		except ImportError:
			print(">ERROR: NOT FOUND: gzip")
			exit()

# Get output name and read from input

	for f in range(0,len(ARGS_input)):
		file=str(ARGS_input[f])
		outname=ARGS_output
		if(f>=1):
			ARGS_output=list(re.compile("([^.]+)\.(.+)").findall(ARGS_output)[0])
			outname="%s_%d.%s"%(ARGS_output[0],f+1,ARGS_output[1])
		if(ARGS_gzip):
			try:INPUT=gzip.open(file,"rb")
			except:
				print(">ERROR: reading %s"%(file))
				exit()
		else:
			try:INPUT=open(file,"rU")
			except:
				print(">ERROR: reading %s"%(file))
				exit()

# Select sequences and print into output file

		if(ARGS_verbose):print(">Input file = %s\n>Output file = %s"%(file,outname))
		if(ARGS_gzip):OUPUT=gzip.open(outname,"wb")
		else:OUPUT=open(outname,"w")
		if(ARGS_verbose):count=0
		isLine=True
		count=0
		while isLine==True:
			lines=list(islice(INPUT,4))
			if(len(lines)==0):isLine=False
			if(random.uniform(0.0,1.0)<=ARGS_probability):
				read=""
				for line in lines:read+=line
				OUPUT.write(read)
				count+=1
				if(ARGS_verbose):print(">Read count = %d"%(count))
				if(count==ARGS_max):isLine=False
		OUPUT.close()
		INPUT.close()
		if(ARGS_verbose):print(">Done!")