#-*- coding: utf-8 -*-

# Add an suffix to the identifiers of each read
# Each read must occupy four lines
# Paired reads must be given in two files

# Author: Denis Jacob Machado
# Email: denisjacobmachado@gmail.com
# Homepage: http://www.ib.usp.br/grant/

# Copyright (C) 2014 - Denis Jacob Machado
# Licence: GNU General Public License version 3.0

def execute(ARGS_input,ARGS_output,ARGS_stanza_size,ARGS_suffix,ARGS_verbose,ARGS_gzip,ARGS_suffix_removal,ARGS_line_break):
	if(ARGS_verbose):
		print("SUFFIXES\n\n>List of input files and suffixes:\n"),
		for f in range(0,len(ARGS_input)):print("--%s <-- %s"%(ARGS_input[f],ARGS_suffix[f]))

# Import libraries

	if(ARGS_gzip):
		try:import gzip
		except ImportError:
			print(">ERROR: NOT FOUND: gzip")
			exit()
	if(ARGS_suffix_removal)or(ARGS_line_break):
		try:import re
		except ImportError:
			print(">ERROR: NOT FOUND: re")
			exit()
	try:from itertools import islice
	except ImportError:
		print(">ERROR: NOT FOUND: itertools:islice")
		exit()

# Read from input file and create the output file

	for f in range(0,len(ARGS_input)):
		file=ARGS_input[f]
		if(f>=1):
			head,tail=list(re.compile("([^.]+)\.(.+)").findall(ARGS_output)[0])
			outname="%s_%d.%s"%(head,f+1,tail)
		else:outname=ARGS_output
		if(ARGS_gzip):
			INPUT=gzip.open(file,"rb")
			OUTPUT=gzip.open(outname,"wb")
		else:
			INPUT=open(file,"r")
			OUTPUT=open(outname,"w")
		if(ARGS_verbose):print(">Input = %s\t-->Output = %s"%(file,outname))

# Read stanzas

		if(ARGS_verbose):count=0
		while True:
			STANZA=list(islice(INPUT,ARGS_stanza_size))
			if(len(STANZA)!=ARGS_stanza_size):break
			else:
				head=STANZA[0]
				tail=STANZA[1:]

# Change end of line

				if(ARGS_line_break):
					try:head=re.compile("(.+)%s"%(ARGS_line_break)).findall(head)[0]
					except:
						print(">ERROR: check arguments (--line_break '%s')"%(ARGS_line_break))
						exit()
				else:head=head.rstrip()

# Suffix removal

				if(ARGS_suffix_removal):
					suffix="%s$"%(ARGS_suffix[f])
					head=re.sub(suffix,"",head)
					head+="\n"
				else:head="%s%s\n"%(head,ARGS_suffix[f])
				print(head.rstrip())

# Resolve stanza

				stanza=head
				for line in tail:stanza+=line
				OUTPUT.write(stanza)
				if(ARGS_verbose):count+=1

# Finish / repeat

		if(ARGS_verbose):print(">Stanza count = %d"%(count))
		OUTPUT.close()
		INPUT.close()

	if(ARGS_verbose):print("Done!")