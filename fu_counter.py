#-*- coding: utf-8 -*-

# Count reads in fasta(.gz)/fastq(.gz) files

# Author: Denis Jacob Machado
# Email: denisjacobmachado@gmail.com
# Homepage: http://www.ib.usp.br/grant/

# Copyright (C) 2014 - Denis Jacob Machado
# Licence: GNU General Public License version 3.0


def execute(ARGS_input,ARGS_stanza_size,ARGS_verbose,ARGS_gzip):
	if(ARGS_verbose):print("COUNTER")

# Import libraries

	try:from itertools import islice
	except ImportError:
		print(">ERROR: NOT FOUND: itertools:islice")
		exit()
	try:import re
	except ImportError:
		print(">ERROR: NOT FOUND: re")
		exit()
	if(ARGS_gzip):
		try:import gzip
		except ImportError:
			print(">ERROR: NOT FOUND: gzip")
			exit()

# Read from input

	for f in range(0,len(ARGS_input)):
		file=str(ARGS_input[f])
		if(ARGS_gzip):INPUT=gzip.open(file,"rb")
		else:INPUT=open(file,"r")

# Count stanzas

		count=0
		if(ARGS_verbose):print(">Input = %s"%(file))
		while True:
			STANZA=list(islice(INPUT,ARGS_stanza_size))
			if(len(STANZA)!=ARGS_stanza_size):break
			else:
				count+=1
				if(ARGS_verbose):print("-- (%d) %s"%(count,STANZA[0].rstrip()))
		result="File %s = %d"%(file,count)
		print(result)
		INPUT.close()