#-*- coding: utf-8 -*-

# What it does: writes read pairs from two separate
# fastq(.gz) files into one single interleaved file

# Assumes that pairs occupy the same line in the two files

# Author: Denis Jacob Machado
# Email: denisjacobmachado@gmail.com
# Homepage: http://www.ib.usp.br/grant/

# Copyright (C) 2014 - Denis Jacob Machado
# Licence: GNU General Public License version 3.0

def execute(ARGS_input,ARGS_output,ARGS_stanza_size,ARGS_verbose,ARGS_gzip):
	if(ARGS_verbose):print("INTERLEAVER")

# Import modules

	try:from itertools import islice
	except ImportError:
		print(">ERROR: NOT FOUND: itertools:islice")
		exit()
	if(ARGS_gzip):
			try:import gzip
			except ImportError:
				print(">ERROR: NOT FOUND: gzip")
				exit()

# Read/create files

	if(ARGS_gzip):
		OUTPUT=gzip.open(ARGS_output,"wb")
		INPUT1=gzip.open(ARGS_input[0],"rb")
		INPUT2=gzip.open(ARGS_input[1],"rb")
	else:
		OUTPUT=open(ARGS_output,"wb")
		INPUT1=open(ARGS_input[0],"rb")
		INPUT2=open(ARGS_input[1],"rb")

	if(ARGS_verbose):print(">File 1 = %s\n>File 2 = %s\n>Output file = %s"%(ARGS_input[0],ARGS_input[1],ARGS_output))
	

# Interleave

	count=0
	isLine=True
	while isLine==True:
		stanza1=list(islice(INPUT1,ARGS_stanza_size))
		stanza2=list(islice(INPUT2,ARGS_stanza_size))
		if(len(stanza1)==0)or((len(stanza2)==0)):isLine=False
		else:
			thisPair=""
			for line in stanza1:thisPair+=line
			for line in stanza2:thisPair+=line
			count+=1
			if(ARGS_verbose):print(">Pairs = %d"%(count))
			OUTPUT.write(thisPair)
	OUTPUT.close()
	INPUT1.close()
	INPUT2.close()
	print(">Total no. of pairs = %d\n>Done!"%(count))