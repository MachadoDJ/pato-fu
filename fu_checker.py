#-*- coding: utf-8 -*-


# Verifies if reads are paired and/or sorted
# Input must be single reads, interleaved paired reads or
# two files with paired reads

# Author: Denis Jacob Machado
# Email: denisjacobmachado@gmail.com
# Homepage: http://www.ib.usp.br/grant/

# Copyright (C) 2014 - Denis Jacob Machado
# Licence: GNU General Public License version 3.0

def execute(ARGS_input,ARGS_verbose,ARGS_checker_opt,ARGS_stanza_size,ARGS_gzip):
	if(ARGS_verbose):print("CHECKER")

# Import libraries
	try:import re
	except ImportError:
		print(">ERROR: NOT FOUND: re")
		exit()
	try:from itertools import islice
	except ImportError:
		print(">ERROR: NOT FOUND: islice in itertools")
		exit()
	if(ARGS_gzip):
		try:import gzip
		except ImportError:
			print(">ERROR: NOT FOUND: gzip")
			exit()

	report="CHECKER :: report\n"
# Single end reads
	if(ARGS_checker_opt==0):
		report+="Option 0 = single end reads\n\nSUMMARY\n\nInput file\tSorted\n"
		for file in ARGS_input:
			count=0
			sorted=True
			if(ARGS_gzip):SOURCE=gzip.open(file,"rb")
			else:SOURCE=open(file,"r")
			STANZA=list(islice(SOURCE,ARGS_stanza_size))
			count+=1
			preid=re.findall(r"[^/_\s]+",STANZA[0])[0]
			while True:
				STANZA=list(islice(SOURCE,ARGS_stanza_size))
				if(len(STANZA)<ARGS_stanza_size):break
				else:count+=1
				id=re.findall(r"[^/_\s]+",STANZA[0])[0]
				if(id<preid):
					sorted=False
					if(ARGS_verbose):print("--%s stopped at read no. %d"%(file,count))
					break
			report+="%s\t%s\n"%(file,str(sorted))
		print(report)

# Interleaved reads
	elif(ARGS_checker_opt==1):
		report+="Option 1 = interleaved reads\n\nSUMMARY\n\nInput file\tSorted\n"
		for file in ARGS_input:
			sorted1=True
			sorted2=True
			paired=True
			count=0
			if(ARGS_gzip):SOURCE=gzip.open(file,"rb")
			else:SOURCE=open(file,"r")
			STANZA1=list(islice(SOURCE,ARGS_stanza_size))
			STANZA2=list(islice(SOURCE,ARGS_stanza_size))
			count+=1
			preid1=re.findall(r"[^/_\s]+",STANZA1[0])[0]
			preid2=re.findall(r"[^/_\s]+",STANZA2[0])[0]
			while True:
				STANZA1=list(islice(SOURCE,ARGS_stanza_size))
				STANZA2=list(islice(SOURCE,ARGS_stanza_size))
				if(len(STANZA1)<ARGS_stanza_size)and(len(STANZA2)<ARGS_stanza_size):break
				elif(len(STANZA1)<ARGS_stanza_size)or(len(STANZA2)<ARGS_stanza_size):
					report+="\nOdd number of reads (not paired?)"
					break
				else:count+=1
				if(ARGS_verbose):print("--pair %d"%(count))
				id1=re.findall(r"[^/_\s]+",STANZA1[0])[0]
				id2=re.findall(r"[^/_\s]+",STANZA2[0])[0]
				if(id1<preid1):sorted1=False
				if(id2<preid2):sorted2=False
				if(preid1!=preid2):paired=False
				if(sorted1==sorted2==paired==False):
					if(ARGS_verbose):print(">WARNING: the program stopped reading the input file because it already knows the reads are unsorted and unpaired. Note that this is not an error.")
					break
		
			report+="%s /1\t%s\n%s /2\t%s\n\nPaired reads = %s"%(file,str(sorted1),file,str(sorted2),paired)
			print(report)

# Paired and reads in two separate files
	elif(ARGS_checker_opt==2):
		report+="Option 2 = paired reads in two separate files\n\nSUMMARY\n\nInput file\tSorted\n"
		if(len(ARGS_input)!=2):
			print(">ERROR: requires exactly 2 input files, %d given"%(len(ARGS_input)))
			exit()
		sorted1=True
		sorted2=True
		paired=True
		count=0
		file1=ARGS_input[0]
		file2=ARGS_input[1]
		if(ARGS_gzip):
			SOURCE1=gzip.open(file1,"rb")
			SOURCE2=gzip.open(file2,"rb")
		else:
			SOURCE1=open(file1,"r")
			SOURCE2=open(file2,"r")
		STANZA1=list(islice(SOURCE1,ARGS_stanza_size))
		STANZA2=list(islice(SOURCE2,ARGS_stanza_size))
		count+=1
		preid1=re.findall(r"[^/_\s]+",STANZA1[0])[0]
		preid2=re.findall(r"[^/_\s]+",STANZA2[0])[0]
		while True:
			STANZA1=list(islice(SOURCE1,ARGS_stanza_size))
			STANZA2=list(islice(SOURCE2,ARGS_stanza_size))
			if(len(STANZA1)<ARGS_stanza_size)and(len(STANZA2)<ARGS_stanza_size):break
			elif(len(STANZA1)<ARGS_stanza_size):
				report+="\nReached the end of file %s (files have different number of reads)"%(file1)
				break
			elif(len(STANZA2)<ARGS_stanza_size):
				report+="\nReached the end of file %s (files have different number of reads)"%(file2)
				break
			else:count+=1
			if(ARGS_verbose):print("--pair %d"%(count))
			id1=re.findall(r"[^/_\s]+",STANZA1[0])[0]
			id2=re.findall(r"[^/_\s]+",STANZA2[0])[0]
			if(id1<preid1):sorted1=False
			if(id2<preid2):sorted2=False
			if(preid1!=preid2):paired=False
			if(sorted1==sorted2==paired==False):
				if(ARGS_verbose):print(">WARNING: the program stopped reading the input files because it already knows the reads are unsorted and unpaired. Note that this is not an error.")
				break
		
		report+="%s\t%s\n%s\t%s\n\nPaired reads = %s"%(file1,str(sorted1),file2,str(sorted2),paired)
		print(report)