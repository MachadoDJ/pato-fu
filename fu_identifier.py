#-*- coding: utf-8 -*-

# Change FASTQ identifier from CASAVA 1.8 format to an older
# Illumina format, and vice-versa

# Author: Denis Jacob Machado
# Email: denisjacobmachado@gmail.com
# Homepage: http://www.ib.usp.br/grant/

# Copyright (C) 2014 - Denis Jacob Machado
# Licence: GNU General Public License version 3.0

def execute(ARGS_input,ARGS_output,ARGS_stanza_size,ARGS_verbose,ARGS_gzip):
	if(ARGS_verbose):
		print("IDENTIFIER\n\n>List of input files:\n"),
		for f in range(0,len(ARGS_input)):print("--%s"%(ARGS_input[f]))

# Import libraries

	if(ARGS_gzip):
		try:import gzip
		except ImportError:
			print(">ERROR: NOT FOUND: gzip")
			exit()
	try:from itertools import islice
	except ImportError:
		print(">ERROR: NOT FOUND: itertools:islice")
		exit()

# Read from input file and create the output file

	for f in range(0,len(ARGS_input)):
		file=ARGS_input[f]
		if(f>=1):
			head,tail=list(re.compile("([^.]+)\.(.+)").findall(ARGS_output)[0])
			outname="%s_%d.%s"%(head,f+1,tail)
		else:outname=ARGS_output
		if(ARGS_gzip):
			INPUT=gzip.open(file,"rb")
			OUTPUT=gzip.open(outname,"wb")
		else:
			INPUT=open(file,"r")
			OUTPUT=open(outname,"w")
		OUTPUT.close()
		if(ARGS_verbose):print(">Input = %s\t-->Output = %s"%(file,outname))

# Read stanzas

		count=0
		STANZA=list(islice(INPUT,ARGS_stanza_size))
		if(len(STANZA)!=ARGS_stanza_size):
			print(">ERROR: could not retreave the first sequence read")
			exit()
		else:
			head=STANZA[0]
			tail=STANZA[1:]
		try:import re
		except ImportError:
			print(">ERROR: NOT FOUND: re")
			exit()
		head=head.rstrip()
		head=re.split(":| |#|/",head)
		head,format=check_format(head,file,ARGS_verbose)
		count=print_result(head,tail,ARGS_gzip,outname,count)
		while True:
			STANZA=list(islice(INPUT,ARGS_stanza_size))
			if(len(STANZA)!=ARGS_stanza_size):break
			else:
				head=STANZA[0]
				tail=STANZA[1:]
				head=head.rstrip()
				head=re.split(":| |#|/",head)
				if(format=="new"):head=downgrade(head)
				elif(format=="old"):head=upgrade(head)
				count=print_result(head,tail,ARGS_gzip,outname,count)
	if(ARGS_verbose):print("Done!")

# Check format according to the number of list items the
# identifier can be separated

def check_format(head,file,ARGS_verbose):
	if(len(head)==11):
		format="new"
		if(ARGS_verbose):print"""--Reads are probably in CASAVA 1.8+ format
	Read 1, file %s
	Instrument name = %s
	Run ID = %s
	Flowcell ID = %s
	Flowcell lane = %s
	Tile number = %s
	'x'-coordinate = %s
	'y'-coordinate = %s
	The member of a pair = %s
	Filter = %s
	Control bits = %s
	Index sequence = %s"""%(str(file),head[0],head[1],head[2],head[3],head[4],head[5],head[6],head[7],head[8],head[9],head[10])
		head=downgrade(head)
	elif(len(head)==7):
		format="old"
		if(ARGS_verbose):print"""--Reads are probably in an older format
	Read 1, file %s
	Instrument name = %s
	Flowcell lane = %s
	Tile number = %s
	'x'-coordinate = %s
	'y'-coordinate = %s
	Index sequence = %s
	The member of a pair = %s"""%(str(file),head[0],head[1],head[2],head[3],head[4],head[5],head[6])
		head=upgrade(head)
	else:
		print">ERROR: identifier format not recognized"
		exit()
	return(head,format)

def print_result(head,tail,ARGS_gzip,outname,count):
	if(ARGS_gzip):
		try:import gzip
		except ImportError:
			print(">ERROR: NOT FOUND: gzip")
			exit()
		OUTPUT=gzip.open(outname,"ab")
	else:OUTPUT=open(outname,"a")
	stanza=head
	for line in tail:stanza+=line
	OUTPUT.write(stanza)
	count+=1
	OUTPUT.close()
	return(count)

def downgrade(head):
	if(not(head[10])):head[10]="0"
	head="%s:%s:%s:%s:%s#%s/%s\n"%(head[0],head[3],head[4],head[5],head[6],head[10],head[7])
	return(head)

def upgrade(head):
	if(head[5]=="0")or(head[5]=="NNNNNN"):head[5]=""
	head="%s:%s:%s:%s:%s:%s:%s %s:%s:%s:%s\n"%(head[0],"000","XXX",head[1],head[2],head[3],head[4],head[6], "N","0",head[5])
	return(head)