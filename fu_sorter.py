#-*- coding: utf-8 -*-


# Sorts stanzas by their first line. Three steps:
# 1. files are divided into several chunks
# 2. chunks are sorted independently
# 3. overlapping segments of different chunks are sorted

# Author: Denis Jacob Machado
# Email: denisjacobmachado@gmail.com
# Homepage: http://www.ib.usp.br/grant/

# Copyright (C) 2014 - Denis Jacob Machado
# Licence: GNU General Public License version 3.0

def execute(ARGS_input,ARGS_output,ARGS_chunk_size,ARGS_stanza_size,ARGS_verbose,ARGS_gzip):
	if(ARGS_verbose):print("SORTER")

# Import libraries

	if(ARGS_gzip):
		try:import gzip
		except ImportError:
			print(">ERROR: NOT FOUND: gzip")
			exit()
	try:import os
	except ImportError:
		print(">ERROR: NOT FOUND: os")
		exit()
	try:import re
	except ImportError:
		print(">ERROR: NOT FOUND: re")
		exit()
	try:from itertools import islice
	except ImportError:
		print(">ERROR: NOT FOUND: islice in itertools")
		exit()

# Read input file

	for f in range(0,len(ARGS_input)):
		if(f>=1):
			ARGS_output=list(re.compile("([^.]+)\.(.+)").findall(ARGS_output)[0])
			ARGS_output="%s_%d.%s"%(ARGS_output[0],f+1,ARGS_output[1])
		file=str(ARGS_input[f])
		if(ARGS_verbose):print(">Check if file is sorted and divide it into chunks with up to %d stanzas each"%(ARGS_stanza_size))
		if(ARGS_gzip):
			try:SOURCE=gzip.open(file,"ra")
			except:
				print(">ERROR: reading %s"%(file))
				exit()
		else:
			try:SOURCE=open(file,"r")
			except:
				print(">ERROR: reading %s"%(file))
				exit()

# General variables

		CHECK=True
		CONTINUE=True
		stanza_count=0
		CHUNK=[]
		ID={}
		lastHeader=""

# Separate chunks

		while CONTINUE==True:
			STANZA=list(islice(SOURCE,ARGS_stanza_size))
			stanza=""
			stanza_count+=1
			if(len(STANZA)<ARGS_stanza_size):
				if(CHUNK):CHUNK,ID=chunks(CHUNK,ID,stanza_count,ARGS_verbose,ARGS_gzip)
				CONTINUE=False
				break
			else:
				if(CHECK==True)and(STANZA[0].rstrip()>lastHeader):lastHeader=STANZA[0].rstrip()
				else:CHECK=False
				for line in STANZA:stanza+=line
			CHUNK+=[stanza]
			if(stanza_count%ARGS_chunk_size==0):CHUNK,ID=chunks(CHUNK,ID,stanza_count,ARGS_verbose,ARGS_gzip)
		if(ARGS_verbose):print("--Stanza count = %d"%(stanza_count-1))
		if(CHECK==True):
			print(">File %s is already sorted\n--removing temporary files..."%(file))
			for i in ID:
				if(ARGS_gzip):os.remove("chunk%d.temp.gz"%(i))
				else:os.remove("chunk%d.temp"%(i))
			print("--Done!")
		SOURCE.close()
		SUPERIMP=superimposed(ID,ARGS_stanza_size,ARGS_output,ARGS_verbose,ARGS_gzip)

def chunks(CHUNK,ID,stanza_count,ARGS_verbose,ARGS_gzip):

# Import libraries

	if(ARGS_gzip):
		try:import gzip
		except ImportError:
			print(">ERROR: NOT FOUND: gzip")
			exit()
	try:import re
	except ImportError:
		print(">Could not import re")
		exit()

# Make chunks

	N=len(ID)+1
	if(ARGS_verbose):print("--chunk %d"%(N))
	CHUNK=sorted(CHUNK)
	ID[N]=[re.compile("([^\n\r]+)").findall(CHUNK[0])[0],re.compile("([^\n\r]+)").findall(CHUNK[len(CHUNK)-1])[0]]
	output="chunk%d.temp"%(N)
	if(ARGS_gzip):TEMP=gzip.open("%s.gz"%(output),"wa")
	else:TEMP=open(output,"w")
	for stanza in CHUNK:TEMP.write(stanza)
	TEMP.close()
	CHUNK=[]
	return CHUNK,ID

def superimposed(ID,ARGS_stanza_size,ARGS_output,ARGS_verbose,ARGS_gzip):

# Import libraries

	if(ARGS_gzip):
		try:import gzip
		except ImportError:
			print(">ERROR: NOT FOUND: gzip")
			exit()
	try:import os
	except ImportError:
		print(">ERROR: NOT FOUND: os")
		exit()
	try:from itertools import islice
	except ImportError:
		print(">ERROR: NOT FOUND: islice in itertools")
		exit()

	if(ARGS_verbose):print(">Sorting chunks")
	HEADERS={}
	for i in ID:
		HEADERS[ID[i][0]]=i
		HEADERS[ID[i][1]]=i
	PATH=[]
	for j in sorted(HEADERS):PATH+=[[HEADERS[j],j]]
	HEADERS={}
	if(ARGS_gzip):OUTPUT=gzip.open(ARGS_output,"wa")
	else:OUTPUT=open(ARGS_output,"w")
	if(ARGS_verbose):it=len(PATH)
	start=""
	if(ARGS_verbose):
		stanza_count=0
		it=len(PATH)
	while(len(PATH)>=2):
		if(ARGS_verbose):print("--Sorting chunks :: iteration %d of %d"%(it+1-len(PATH),it-1))
		CHUNK=[]
		opt1=ID[PATH[1][0]][1]
		try:opt2=PATH[2][1]
		except:opt2=opt1
		end=min(opt1,opt2)
		for i in ID:
			if(start<ID[i][1])and(end>ID[i][0]):
				if(ARGS_gzip):SOURCE=gzip.open("chunk%d.temp.gz"%(i),"ra")
				else:SOURCE=open("chunk%d.temp"%(i),"r")
				STANZA=list(islice(SOURCE,ARGS_stanza_size))
				try:head=STANZA[0].rstrip()
				except:break
				while(head<=end):
					if(head>=start):
						stanza=""
						for j in STANZA:stanza+=j
						CHUNK+=[stanza]
						if(ARGS_verbose):stanza_count+=1
					STANZA=list(islice(SOURCE,ARGS_stanza_size))
					try:head=STANZA[0].rstrip()
					except:break
				SOURCE.close()
		for i in sorted(CHUNK):OUTPUT.write(i)
		PATH=PATH[1:]
		start=end
	CHUNK=[]
	OUTPUT.close()
	if(ARGS_verbose)and(ARGS_gzip):print("--Sorted chunks written into %d.fastq.gz\n--Stanza count = %d"%(ARGS_output,stanza_count))
	elif(ARGS_verbose):print("--Sorted chunks written into %s.fastq\n--Stanza count = %d"%(ARGS_output,stanza_count))
	if(ARGS_verbose):print("--Removing temporary files...")
	for i in ID:
		if(ARGS_gzip):os.remove("chunk%d.temp.gz"%(i))
		else:os.remove("chunk%d.temp"%(i))
	if(ARGS_verbose):print(">All done!")