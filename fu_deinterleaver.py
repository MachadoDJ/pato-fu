#-*- coding: utf-8 -*-

# What it does: De-interleave fastq(.gz) files

# Write the first four line in output file no. 1, the next
# four lines in output file no. 2, and repeat until there is
# no more reads left

# Author: Denis Jacob Machado
# Email: denisjacobmachado@gmail.com
# Homepage: http://www.ib.usp.br/grant/

# Copyright (C) 2014 - Denis Jacob Machado
# Licence: GNU General Public License version 3.0

def execute(ARGS_input,ARGS_output,ARGS_stanza_size,ARGS_verbose,ARGS_gzip):
	if(ARGS_verbose):print("DEINTERLEAVER")

# Import modules

	try:import re
	except ImportError:
		print(">ERROR: NOT FOUND: itertools:islice")
		exit()
	try:from itertools import islice
	except ImportError:
		print(">ERROR: NOT FOUND: itertools:islice")
		exit()
	if(ARGS_gzip):
		try:import gzip
		except ImportError:
			print(">ERROR: NOT FOUND: gzip")
			exit()

# Resolve file names

	prefix,sufix=re.compile("([^.]+)\.(.+)").findall(ARGS_output)[0]
	out1="%s_1.%s"%(prefix,sufix)
	out2="%s_2.%s"%(prefix,sufix)
	file=ARGS_input[0]
	if(ARGS_verbose):print(">Input file = %s\n>Output file 1 = %s\n>Output file 2 = %s"%(file,out1,out2))

# Read from input file and create output files

	if(ARGS_gzip):
		INPUT=gzip.open(file,"ra")
		OUTPUT1=gzip.open(out1,"wa")
		OUTPUT2=gzip.open(out2,"wa")
	else:
		INPUT=open(file,"r")
		OUTPUT1=open(out1,"w")
		OUTPUT2=open(out2,"w")
		count_stanzas=0
		while True:
			STANZA=list(islice(INPUT,ARGS_stanza_size))
			if(len(STANZA)==0):break
			else:
				stanza=""
				for line in STANZA:stanza+=line
				count_stanzas+=1
				if(ARGS_verbose):print("Stanza count = %d"%(count_stanzas))
				if((count_stanzas%2)==0):OUTPUT2.write(stanza)
				else:OUTPUT1.write(stanza)
		OUTPUT1.close()
		OUTPUT2.close()

# Final message

		print(">Total no. of stazas = %d (%d per file)\n>Done!"%(count_stanzas,count_stanzas/2))